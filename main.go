package crius_plugin_info

import (
	"context"
	"database/sql"
	"errors"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Info",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-info/",
	Description:        "Information plugin (this is sort of like a custom command system, albeit w/o the custom activators)",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			HandlerName:        "info",
			Name:               "Get Info",
			Help:               "Commands to configure and use the information storage system",
			Activator:          "info",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
			Subcommands: []*cc.PJCommand{
				{
					HandlerName:        "add",
					Name:               "Add Info",
					Help:               "Add an information item to the storage. Usage: `info add [name] [info]`",
					Activator:          "add",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
				{
					HandlerName:        "get",
					Name:               "Get Info",
					Help:               "Get an info item. Usage: `info get [name]`",
					Activator:          "get",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
				{
					HandlerName:        "list",
					Name:               "List Items",
					Help:               "Get a list of all the info items for the server.",
					Activator:          "list",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
				{
					HandlerName:        "remove",
					Name:               "Remove Info",
					Help:               "Remove an info item. Usage: `info remove [name]`",
					Activator:          "remove",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
			},
		},
		{
			HandlerName:        "rules",
			Name:               "Rules",
			Help:               "Get this realm's rules",
			Activator:          "rules",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
			Subcommands: []*cc.PJCommand{
				{
					HandlerName:        "set",
					Name:               "Set Rules",
					Help:               "Set this realm's rules. Usage: `rules set [content]`",
					Activator:          "set",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
				{
					HandlerName:        "clear",
					Name:               "Clear Rules",
					Help:               "Clear this realm's rules",
					Activator:          "clear",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
				},
			},
		},
	},
}

type info struct {
	db *sqlx.DB
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := CriusUtils.GetSettings(ctx)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	i := &info{
		db: db,
	}

	return map[string]cc.CommandHandler{
		"info/add":    i.AddInfo,
		"info/get":    i.Info,
		"info/list":   i.InfoItems,
		"info/remove": i.RemInfo,
		"rules":       i.Rules,
		"rules/set":   i.SetRules,
		"rules/clear": i.ClearRules,
	}, nil
}

func (i *info) AddInfo(m *cc.MessageContext, args []string) error {
	if len(args) < 2 {
		m.Send("you must specify a name and some info. Usage: `info add [name] [info]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can add new info.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can add new info.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can add new info.")
				return nil
			}
		}
	}

	_, err := i.db.Exec("INSERT INTO info__info (realm_id, platform, info_name, info) VALUES ($1, $2, $3, $4)",
		realmID, platform.ToString(), args[0], args[1])
	if err != nil {
		return err
	}

	m.Send("Added info item " + args[0])

	return nil
}

func (i *info) Info(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a item name, do `info list` for a list.")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	info := &InfoItem{}
	err := i.db.Get(info, "SELECT * FROM info__info WHERE platform=$1 AND realm_id=$2 AND info_name=$3",
		platform.ToString(), realmID, args[0])
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if errors.Is(err, sql.ErrNoRows) {
		m.Send("there is no item with the name " + args[0])
		return nil
	}

	m.Send(info.InfoName + ": " + info.Info)

	return nil
}

func (i *info) InfoItems(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	// pull any items from the db
	infoItems := []*InfoItem{}
	err := i.db.Select(&infoItems, "SELECT * FROM info__info WHERE realm_id=$1 AND platform=$2", realmID, platform.ToString())
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	// apparently we won't get a ErrNoRows, but I've seen it in the past so just in case.
	if errors.Is(err, sql.ErrNoRows) || len(infoItems) == 0 {
		m.Send("this realm has no info stored.")
		return nil
	}

	// concat and send.
	msg := "This realm has the following info items: "
	for i, item := range infoItems {
		msg += item.InfoName
		if i < len(infoItems)-1 {
			msg += ", "
		}
	}

	m.Send(msg)

	return nil
}

func (i *info) RemInfo(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a item name. Usage: `info remove [name]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can remove info items.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can remove info items.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can remove info items.")
				return nil
			}
		}
	}

	_, err := i.db.Exec("DELETE FROM info__info WHERE realm_id=$1 AND platform=$2 AND info_name=$3",
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send("Removed item " + args[0])

	return nil
}

func (i *info) SetRules(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify the content. If you are trying to clear the rules, use `rules clear`. Usage `rules set [content]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can set the rules.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can set the rules.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can set the rules.")
				return nil
			}
		}
	}

	_, err := i.db.Exec(`INSERT INTO info__rules (realm_id, platform, rules)
		VALUES ($1, $2, $3) ON CONFLICT ON CONSTRAINT info__rules__pk DO UPDATE SET rules=$3`,
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send("I have set the rules.")

	return nil
}

func (i *info) Rules(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	// pull the rules from the db
	var rules string
	err := i.db.Get(&rules, "SELECT rules FROM info__rules WHERE realm_id=$1 AND platform=$2", realmID, platform.ToString())
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	// apparently we won't get a ErrNoRows, but I've seen it in the past so just in case.
	if errors.Is(err, sql.ErrNoRows) {
		m.Send("this realm has no rules stored.")
		return nil
	}

	// concat and send.
	m.Send("This realm's rules: " + rules)

	return nil
}

func (i *info) ClearRules(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can clear the rules.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can clear the rules.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can clear the rules.")
				return nil
			}
		}
	}

	_, err := i.db.Exec("DELETE FROM info__rules WHERE realm_id=$1 AND platform=$2",
		realmID, platform.ToString())
	if err != nil {
		return err
	}

	m.Send("Cleared the rules")

	return nil
}
